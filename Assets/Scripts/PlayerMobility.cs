﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMobility : MonoBehaviour {

	public float speed;
	public Rigidbody2D player;

	void Start() {
		player = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		var mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Quaternion rot = Quaternion.LookRotation (transform.position - mousePosition, Vector3.forward);

		transform.rotation = rot;
		transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
		player.angularVelocity = 0;

		float input = Input.GetAxis ("Vertical");
		player.AddForce (gameObject.transform.up * speed * input);

		
	}



}
