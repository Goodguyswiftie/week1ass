﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerTrigger : MonoBehaviour {
	public float mCarried = 3;
	public Text inHeliTxt;
	public Text rescuedTxt;
	public Text resultTxt;
	private float pCarried;
	private float pDelivered;
	private bool isLost;
	public AudioClip pickup;
	// Use this for initialization
	void Start () {
		pCarried = 0;
		pDelivered = 0;
		setInHeliTxt ();
		setRescuedTxt ();
		isLost = false;
		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = pickup;
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.R)) {
			isLost = false;
			Time.timeScale = 1;
			SceneManager.LoadScene (0);
		}
	}
	void FixedUpdate() {
		if (isLost == true) {
			resultTxt.text = "You lose!";
			Time.timeScale = 0;

		}

		if (pDelivered == 5) {
			resultTxt.text = "You win!";
			Time.timeScale = 0;
		}
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "tree") {
			isLost = true;
		} else if (other.tag == "person") {
			if (pCarried < mCarried) {
				GetComponent<AudioSource> ().Play ();
				pCarried++;
				setInHeliTxt();
				Destroy (other.gameObject);

			}
		} else if (other.tag == "hospitala") {
			pDelivered = pDelivered + pCarried;
			pCarried = 0;
			setInHeliTxt ();
			setRescuedTxt ();
		}
	}

	void setInHeliTxt() {
		inHeliTxt.text = "Soldiers in Helicopter: " + pCarried.ToString ();
	}
	void setRescuedTxt() {
		rescuedTxt.text = "Soldiers Rescued: " + pDelivered.ToString ();	
	}
}
